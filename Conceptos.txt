Serializar: Convertir un dato (objeto, array, estructura, etc...) de la memoria RAM (almacenado en binario) a un formato transferible o legible o almacenable en disco

Deserializar: Lo contrario, de formato legible a binario en RAM

API: Application Program Interface

P.O.O.: Programación Orientada a Objetos:
- Programación estructurada (for, if, instruciones, funciones...)
	- Clases: que son como plantillas/moldes/estruturas 
	- Objetos: Instancias de las clases, objetos concretos con la estructura de la clase pero con sus propios valores en sus propiedades.
	- Método: Es un tipo de función que da funcionalidad a una clase
	- Herencia: Una clase puede heredad las propiedades y los métodos de otra clase.
	- Encapsulación: Capacidad de modificar el nivel de acceso de clases, métodos, propiedades, etc... private y public
	- Polimorfismo: ...
	
	
	
	