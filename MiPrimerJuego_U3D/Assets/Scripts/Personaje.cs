﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    GameObject sprite;
    // float es un número decimal
    public float velocidad = 40;    // 40 es valor x defecto

    private void Start()
    {
        sprite = transform.GetChild(0).gameObject;
    }
    // Update is called once per frame
    void Update()
    {
        // Cuando se pulsa <--

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Mover(-1);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Mover(+1);
        }
    }
    public void Mover(int direccion)
    {
        sprite.transform.localScale = new Vector3(direccion, 1, 1);
        Vector3 vDireccion;
        if (direccion > 0)
            vDireccion = Vector3.right;
        else if (direccion < 0)
            vDireccion = Vector3.left;
        else
            vDireccion = Vector3.zero;

        Vector3 vectorMov = velocidad * vDireccion * Time.deltaTime;

        this.GetComponent<Transform>().Translate(vectorMov);

        if (this.GetComponent<Transform>().position.x > 10)
        {
            // entonces recolocamos en el margen izq
            this.GetComponent<Transform>().position = new Vector3(10, 0, 0);
            Debug.Log("Choque a la izquierda: ");
        }
        // Si la posición en el eje X es menor que -10 (margen izq)
        if (this.GetComponent<Transform>().position.x < -10)
        {
            // entonces recolocamos en el margen izq
            this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
            Debug.Log("Choque a la izquierda: ");
        }
    }
}
