﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MandoJugador : MonoBehaviour
{
    public GameObject jugador;

    public int direccion = 1;
    public bool pulsando;
    private void Update()
    {
        if (pulsando)
        {
            jugador.GetComponent<Personaje>().Mover(direccion);
        }
    }
    private void OnMouseDown()
    {
        pulsando = true;
    }
    private void OnMouseUp()
    {
        pulsando = false;
    }
    private void OnMouseExit()
    {
        pulsando = false;
    }
}
