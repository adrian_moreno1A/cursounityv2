﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOfLiveScript : MonoBehaviour
{
    // Imagen original o semilla enlazada en Unity
    public Texture2D planoSemilla;
    // Imagen generada por código
    public Texture2D plano_1;
    public Texture2D plano_2;
    // Plano en la escena 3D
    public GameObject quad;

    // Start is called before the first frame update
    void Start()
    {
        // Si tenemos imagen enlazada
        if (planoSemilla != null)
        {
            planoSemilla.SetPixel(
                planoSemilla.width - 2, 
                planoSemilla.height - 2,
                Color.white);

            planoSemilla.Apply();
            plano_2 = new Texture2D(planoSemilla.width,
                planoSemilla.height,
                TextureFormat.RGB24, false);
            plano_2.anisoLevel = 0;
            // plano_2.alphaIsTransparency = false;
            plano_2.Apply();

            // Recorremos en bucle toooooda la imagen, de izq a derecha
            for (int x = 0; x < plano_2.width; x++)
            {
                // Y de arriba abajo
                for (int y = 0; y <plano_2.height; y++)
                {
                    // Y la pintamos de negro

                    plano_2.SetPixel(x, y, Color.black);
                }
            }
            plano_2.Apply();

            quad.GetComponent<Renderer>().material.mainTexture = plano_2;
        }

        int v = CuantosVecinosVivos(planoSemilla, 1, 1);
        Debug.Log("Vivos: " + v);
    }

    // Update is called once per frame
    void Update()
    {
        // Ahora por cada frame, generamos una nueva imagen (plano_2)
        // que generamos a partir de plano_semilla, aplicando las reglas
        // del juego de la vida de J.H. Conway
        for (int x = 0; x < plano_2.width; x++)
        {
            // Y de arriba abajo
            for (int y = 0; y < plano_2.height; y++)
            {
                // Y la pintamos de negro
                plano_2.SetPixel(x, y, Color.black);
                // Comprobamos si la celula revive, sólo las que son negras
                if (planoSemilla.GetPixel(x, y) == Color.black)
                {
                    // Revivimos cuando tenga 3 vecinas vivas
                    if ( CuantosVecinosVivos(planoSemilla, x, y) == 3)
                    {
                        // Revivimos la célula
                        plano_2.SetPixel(x, y, Color.white);
                    }
                }
                if (planoSemilla.GetPixel(x, y) == Color.white)
                {
                    if (CuantosVecinosVivos(planoSemilla, x, y) == 2
                        || CuantosVecinosVivos(planoSemilla, x, y) == 3)
                    {
                        // Mantenemos viva la célula
                        plano_2.SetPixel(x, y, Color.white);
                    }
                }
            }
        }
        plano_2.Apply();
        //TODO: Copiar todo el plano_2 en el planoSemilla para la siguiente pasada (frame, Update())
        /*for (int x = 0; x < plano_2.width; x++)
        {
            // Y de arriba abajo
            for (int y = 0; y < plano_2.height; y++)
            {
                // Y la pintamos de negro
                planoSemilla.SetPixel(x, y, plano_2.GetPixel(x, y));
            }
        }*/
        planoSemilla.SetPixels(plano_2.GetPixels());
        planoSemilla.Apply();
    }
    // "px" e "py" Estos son parámetros de la función, es decir, variables
    // locales que se reciben cuando se llama (invoca) a la función
    int CuantosVecinosVivos(Texture2D tex, int px, int py)   
    {
        int cuentaVivos = 0;
        for (int x = px - 1; x <= px + 1; x++)
        {
            // Y de arriba abajo
            for (int y = py - 1; y <= py + 1; y++)
            {
                // Preguntamos si el pixel está vivo siempre y cuando
                // no sea el propio pixel recibido
                if (px != x || py != y)   // ! ( px == x && py == y )
                    if (tex.GetPixel(x, y) == Color.white)
                        cuentaVivos = cuentaVivos + 1;
            }
        }
        return cuentaVivos;
    }
}
