﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Comentario de pruba
namespace GermanSTS
{
    // Clase ControladorEscenas
    public class ControladorEscenas : MonoBehaviour
    {
        [Header("Propiedades ctrl escenas:")]
        public string nombreEscena;

        private void Start()
        {
            if (nombreEscena != "")
            {
                print("Arrancando escena " + nombreEscena);
                this.CargarEscena(nombreEscena);
            }
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
        public void CargarEscena(string escena)
        {
            SceneManager.LoadScene(escena);
        }
    }
}
