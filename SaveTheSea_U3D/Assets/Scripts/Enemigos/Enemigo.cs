﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public float velocidad;
    // Dos objetos/componentes a enlazar:
   //  private ControladorJuego ctrolJuego;   // 
    GameObject jugador;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position += movAbajo;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Sólo si este componente está habilitado
        if (this.enabled)
        {
            Debug.Log("Enemigo colisionado con " + collision.gameObject.name);
            if (collision.gameObject.name.ToLower().Contains("fondo"))
            {
                // Deshabilitar este mismo componente Enemigo.cs
                this.enabled = false;
                // Detener la animación
                this.GetComponent<Animator>().speed = 0;
            }
            // Que cuando choque con jugador, que destruya el enemigo (buscar cómo destruir un game object)
            if (collision.gameObject.name.ToLower().Contains("cuerpo"))
            {
                Destroy(this.gameObject);
            }
        }
    }
}
