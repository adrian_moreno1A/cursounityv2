using System;

/*	Estos son los tipos primitivos de C# 
	(igual que en otros lenguajes de prog.)
	
*/
public class TiposDeDatos 
{
	public static void Main() {
		byte unByte = 255;	// Nº entero entre 0 y 255, 8 bits, 2^8
		char unCaracter = 'A';	// Un caracter tb es un num		
		int numEntero = 1000;	// N?mero entre -2.000.000.000 y +2.000.000.000
								// porque ocupa 4 bytes. 2^32 = 4.000.000.000
		
		Console.WriteLine("Byte: " + unByte + " char: " + unCaracter);
		Console.WriteLine("Un char ocupa " + sizeof(char) + " bytes");
		Console.WriteLine("El entero vale " + numEntero);
		Console.WriteLine("Y ocupa " + sizeof(int) + " bytes");
		numEntero = 2000000000;	// 2 mil millones max
		Console.WriteLine("Ahora el entero vale " + numEntero);
		
		// para guardar n?meros m?s largos:
		long enteroLargo = 500000000000L;	// 8 bytes
		Console.WriteLine("Entero largo vale " + enteroLargo);
		
		// Tipos de decimales: float (4 bytes) y double (8 bytes)
		// Precisi?n es de:   7-8 cifras.		  15-16 cifras en TOTAL
		float numDecimal = 123.456789F;
		Console.WriteLine("Num decimal float vale " + numDecimal);
		// Para m?s precisi?n: double
		double numDoblePrecision = 12345.67890123456789;
		Console.WriteLine("Num decimal doble vale " + numDoblePrecision);
				
		// Para guardar Si/No, Verdad/Falso, Cero/Uno...
		bool variableBooleana;	// 1 ?nico byte
		variableBooleana = true;
		Console.WriteLine("variableBooleana vale " + variableBooleana);
		// podemos guardar comparaciones, condiciones, etc...
		variableBooleana =   numDecimal > 1000;	
		Console.WriteLine("variableBooleana ahora es " + variableBooleana);
		variableBooleana = numDecimal <= 1000;
		Console.WriteLine("variableBooleana ahora es " + variableBooleana);

		string cadenaDeTexto = "Pues eso, una cadena de texto";
		Console.WriteLine(cadenaDeTexto);
		Console.WriteLine(cadenaDeTexto + " que " + " permite concatenacion");
		// Lo que no permite son conversiones inv?lidas:
		string otroTxt = "" + numEntero;
	}
}



