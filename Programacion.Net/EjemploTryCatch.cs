﻿using System;

public class EjemploTryCatch
{
	static void Main()
    {
        // int x = Int32.Parse("NO ES NUM");    // Lanzará una excepción
        try
        {
            Console.WriteLine("Empieza el programa");
            int x = Int32.Parse("NO ES NUM");    // Lanzará una excepción
            Console.WriteLine("Esta línea no se ejecuta");
        }
        catch (FormatException error)
        {
            Console.WriteLine("Podemos diferenciar entre tipos de errores.");
            Console.WriteLine("ERROR DE FORMATO");
            Console.WriteLine(error.Message);
        }
        catch (Exception error)
        {
            Console.WriteLine("Error genérico");
            Console.WriteLine(error.Message);
        }
        Console.WriteLine("Pero al capturar el error, sí permitimos continuar el programa");
        Console.WriteLine("Incluso mostrar el error de manera controlada. ");
        EjercicioArrayTryCatch();
    }
    // Provocar un error de límites de array. Es decir, creamos un array de 5 elementos y 
    // intentamos acceder al 7º (que no existe, provocará un error). Luego, controlarlo con Try / Catch
    static void EjercicioArrayTryCatch()
    {
        Console.WriteLine("Podemos diferenciar entre tipos de errores.");
        try
        {
            Console.WriteLine("Provocar error array");
            int[] arrayCualquiera = { 1, 2, 3, 4, 5 };
            arrayCualquiera[7] = 20;
        }
        catch (FormatException error)
        {
            Console.WriteLine("ERROR DE FORMATO");
            Console.WriteLine(error.Message);
        }
        catch (IndexOutOfRangeException error)
        {   // Se capturá el error en este catch
            Console.WriteLine("ERROR DE LIMITE DE ARRAYS");
            Console.WriteLine(error.Message);
        }
        catch (Exception error)
        {
            Console.WriteLine("Error genérico");
            Console.WriteLine(error.Message);
        }
    }
}
