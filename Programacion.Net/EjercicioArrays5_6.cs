using System;

	// Ejercicio 5: Crear una función que calcule la media de todos los ataques
	// Ejercicio 6: Crear una función que calcule el mínimo de los ataques de los enemigos CERCANOS.
	// Ejercicio 7: Crear una función que devuelva un array que contenga el doble de los ataques:  { 1.6f, 0.85f, ....}
	
public class EjemploArrays5_6 
{	
	public static void Main() 
	{
		EjercicioArrayAtaques(); 
	}	
	public static void EjercicioArrayAtaques() 
	{
		float[] ataquesEnem = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool[] siEnemCercanos = {false, true, true, false, true, true};
		
		// Ejercicio 5: Mostrar ataque medio
		float mediaAtaques;
		mediaAtaques = CalcMediaAtaques( ataquesEnem ) ;
		Console.WriteLine("Media = " + mediaAtaques);	
	}
	public static float CalcMediaAtaques(float[] ataques)
	{
		return CalcSumaAtaques(ataques) / ataques.Length;
	}
	public static float CalcSumaAtaques(float[] ataques)
	{
		float  sumaTotal = 0;
		
		for (int i = 0; i < ataques.Length; i++) 
		{
			sumaTotal = sumaTotal + ataques[i];
		}
		return sumaTotal;
	}
}



