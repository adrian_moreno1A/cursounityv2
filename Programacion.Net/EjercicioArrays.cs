using System;

public class EjemploArrays 
{	
	public static void Main() 
	{
		EjercicioArrayAtaques(); 
	}
	
	public static void EjercicioArrayAtaques() 
	{
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool[] siCercanos = {false, true, true, false, true, true};
		
		float totalAtaques;
		totalAtaques = CalcAtaqueTotalCercanos( ataques, siCercanos );
				
		bool[] siCercanosJug2 = {true, true, false, false, true, false};
		float totalAtqJug2 = CalcAtaqueTotalCercanos( ataques, siCercanosJug2 );
		
		Console.WriteLine("Total = " + totalAtaques);
		Console.WriteLine("Total = " + totalAtqJug2);
		
		// Ejercicio 3:
		float totalMay_3 = CalcAtaqueMayoresQueTope(ataques, 3f);
		Console.WriteLine("Total de > 3 = " + totalMay_3);
		
		// Ejercicio 4:
		Console.WriteLine("El max: " + CalcAtaqueMaximo(ataques) );
	}
	
	public static float CalcAtaqueTotalCercanos(float[] ataques, bool[] siCercanos)
	{		
		float total;
		
		total = 0;	// Inicializar variables!!
		
		// Para cuando los ataques sumen sean los verdaderos
		for (int i = 0; i < ataques.Length; i = i + 1) 
		{
			Console.WriteLine("Valor de " + i + " es " + ataques[i]);
			if (siCercanos[i]) {
				total = total + ataques[i];
			}
		}
		return total;
	}
	
	public static float CalcAtaqueMayoresQueTope(float[] ataques, float tope) 
	{
		float total = 0;
		int i = 0;
		for ( ; i < ataques.Length;  ) {
			if (ataques[i] > tope) {
				total += ataques[i];
			}
			i++;
		}
		return total;
	}
	public static float CalcAtaqueMaximo(float[] ataques)
	{
		float valMax = ataques[0];
		
		for (int i = 0; i < ataques.Length; i++) 
		{
			if ( ataques[i] > valMax ) {
				valMax = ataques[i];
			}
		}
		return valMax;
	}
	
	// Ejercicio 2: Crear una función que genere (devuelva, return) un array con textos con la info de los NO cercanos:
	// El resultado es un array: {"Enemigo 0: 3.2", "Enemigo 3: 5.0f"}
	
	// Ejercicio 3: Crear una función que calcule el ataque total de los que tengan ataque > 3
	// 				este tope (3) debe pasarse por argumento/parámetro
	
	// Ejercicio 4: Otra que devuelva el ataque máximo del array	(resultado: 7.1 es máx)
	
	// Ejercicio 5: Crear una función que calcule la media de todos los ataques
	// Ejercicio 6: Crear una función que calcule el mínimo de los ataques de los enemigos CERCANOS.
	// Ejercicio 7: Crear una función que devuelva un array que contenga el doble de los ataques:  { 1.6f, 0.85f, ....}
}




