using System;

public class PasoPorValorPorReferencia
{
    static void Main()
    {
        Console.WriteLine("Paso por valor:");
        int y = 10;
        CambiarVariablePorValor(y);
        Console.WriteLine("Y = " + y);

        CambiarVariablePorReferencia(ref y);
        Console.WriteLine("Y = " + y);

        // Ejemplo de paso por valor
        // Como puede "cascar" hay que controlar la excepci�n con try / catch
        string supuestoNumero_1 = "200", supuestoNumero_2 = "LOO";
        int numero_1 = 0, numero_2 = 0;
        try
        {
            numero_1 = Int32.Parse(supuestoNumero_1);   // BIEN
            numero_2 = Int32.Parse(supuestoNumero_2);   // FALLA
        }
        catch (Exception ex)
        {
            Console.WriteLine("Casca en el formateo " + ", Num 2 = " + numero_2);
            Console.WriteLine("Casca en el formateo Num 2 = " + ex.Message);
        }
        // Ejemplo de paso por referencia
        if (Int32.TryParse(supuestoNumero_1, out numero_1))
        {
            Console.WriteLine("OK, Num 1 = " + numero_1);
        } else
        {
            Console.WriteLine("Casca en el formateo");
        }
        if (Int32.TryParse(supuestoNumero_2, out numero_2))
        {
            Console.WriteLine("OK, Num 2 = " + numero_2);
        }
        else
        {
            Console.WriteLine("Casca en el formateo");
        }
    }
    // Pasamos el dato POR VALOR: Se hace una copia en memoria
    // Las variables (la anterior, y este par�metro) 
    // representan variables diferentes (s�lo se copia el valor)
    static void CambiarVariablePorValor(int x)
    {
        x = 20;
    }
    // Las variables (la anterior, y este par�metro) 
    // representan la MISMA VARIABLE (se copia la referencia, direcci�n de memoria)
    static void CambiarVariablePorReferencia(ref int z) // tambi�n vale out
    {
        z = 20;
    }
}