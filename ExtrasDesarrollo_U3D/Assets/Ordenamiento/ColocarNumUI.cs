﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColocarNumUI : MonoBehaviour
{ 
    public List<Text> numeros;
    List<int> listaNum;
    int menorDeTodos;

    // Start is called before the first frame update
    void Start()
    {
        listaNum = new List<int>();
        foreach (Text txtNum in numeros)
        {
            int num = Int32.Parse(txtNum.text);
            listaNum.Add(num);
        }
        menorDeTodos = OrdenarComoHumano.Menor(listaNum);
        OrdenarComoHumano.MostrarLista(listaNum);
    }

    // Update is called once per frame
    void Update()
    {

    } 
    //TODO: Para usar la función estática, falta algo
    public static int Menor(List<int> lista)
    {
        int menor = lista[0];
        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] < menor)
            {
                menor = lista[i];
            }
        }
        Debug.Log("Número menor: " + menor);
        return menor;
    }
}
